 /*la fonction getElementByTagName renvoie une liste des éléments portant le nom de balise donné ici "span".*/
 /* the function getElementByTagName returns a list of elements with the tag name given here "span". */

 var sp = document.getElementsByTagName("span");
 var btn_start=document.getElementById("start");
 var btn_stop=document.getElementById("stop");
 var btn_save=document.getElementById("save");
 var ms=0,s=0,mn=0,h=0;
 var t;
 var btn_save=document.getElementById("save");
 var list=document.getElementById("list"); 

 document.querySelector("#start").addEventListener("click", start);
 document.querySelector("#stop").addEventListener("click", stop);
 document.querySelector("#reset").addEventListener("click", reset);
 document.querySelector("#save").addEventListener("click", save);
  
  /*La fonction "start" démarre un appel répétitive de la fonction update_chrono par une cadence de 100 milliseconde en utilisant setInterval et désactive le bouton "start" */
 /* The "start" function starts a repetitive call of the update_chrono function at a rate of 100 milliseconds using setInterval and deactivates the "start" button */
  
  function start(){
  t =setInterval(update_chrono,100);
  btn_start.disabled=true;
 }

 /*La fonction update_chrono incrémente le nombre de millisecondes par 1 <==> 1*cadence = 100 */
 /* The update_chrono function increments the number of milliseconds by 1 <==> 1 * rate = 100 */
 
 function update_chrono(){
   ms+=1;
   /*si ms=10 <==> ms*cadence = 1000ms <==> 1s alors on incrémente le nombre de secondes*/
      if(ms==10){
       ms=1;
       s+=1;
      }
      /*on teste si s=60 pour incrémenter le nombre de minute*/
      /* we test if s = 60 to increment the number of minutes */
      if(s==60){
       s=0;
       mn+=1;
      }
      if(mn==60){
       mn=0;
       h+=1;
      }
      /*afficher les nouvelle valeurs*/
      /* show the new values */
      sp[0].innerHTML=h+" h";
      sp[1].innerHTML=mn+" min";
      sp[2].innerHTML=s+" s";
      sp[3].innerHTML=ms+" ms";
}
 
   /*on arrête le "timer" par clearInterval ,on réactive le bouton start */
   /* we stop the "timer" by clearInterval, we reactivate the start */
   function stop(){
   clearInterval(t);
   btn_start.disabled=false;
       
}
 /*dans cette fonction on arrête le "timer" ,on réactive le bouton "start" et on initialise les variables à zéro */
 /* in this function one stops the "timer", one reactivates the button "start" and one initialises the variables with zero */
 function reset(){
  clearInterval(t);
   btn_start.disabled=false;
   ms=0,s=0,mn=0,h=0;


   /*on accède aux différents span par leurs indice*/
    /* we access the different span by their index */
   sp[0].innerHTML=h+" h";
   sp[1].innerHTML=mn+" min";
   sp[2].innerHTML=s+" s";
   sp[3].innerHTML=ms+" ms";

   console.log(list) ; 
   document.querySelector("#list").innerHTML = "" ;
   
}

/* Save the data in a table */
function save() {
     list.innerHTML += "<li>" + h + " h : " + mn + " mn : " + s + " s : " + ms + " ms " + "</li>" + "<br>";
}



